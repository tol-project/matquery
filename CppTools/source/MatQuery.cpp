/* CppTools.cpp: C++ API to invoke GSL from TOL

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bsetgra.h>

#define dMat(arg) ((DMat&)Mat(arg))

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockMatQuery()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}


bool GetIndexes(const char *argName, BSyntaxObject *arg, BArray<int> &rows)
{
  std::vector<int> indexes;
  if(arg->Grammar() == GraMatrix())
    {
    DMat& argM = dMat(arg);
    for(int i = 0; i < argM.Rows(); i++)
      {
      double x = argM(i,0);
      if (x!=0)
        {
        indexes.push_back(i);
        }
      }
    }
  else if (arg->Grammar() == GraVMatrix())
    {
    BVMat& argM = VMat(arg);
    for(int i = 0; i < argM.Rows(); i++)
      {
      double x = argM.GetCell(i,0);
      if (x!=0)
        {
        indexes.push_back(i);
        }
      }
    }
  else
    {
    char msgBuffer[ 512 ];
    snprintf(msgBuffer, sizeof(msgBuffer),
             "wrong type for argument %s must be Matrix or VMatrix", argName);
    Error( msgBuffer );    
    return false;
    }
  int r = indexes.size();
  rows.AllocBuffer(r);
  for(int i=0; i<r; i++)
    {
    rows[i] = indexes[i];
    }
  return true;
}

//--------------------------------------------------------------------
DeclareContensClass(BVMat, BVMatTemporary, BVMatExtractRowByMask);
DefMethod(1, BVMatExtractRowByMask, "VMatSubRowByMask", 2, 2, "VMatrix Anything",
  I2("(VMatrix M , Anything rowsMask)",
     "(VMatrix M , Anything indicadoresDeFila)"),
  I2("Selects from the matrix M the indicated rows in rowsMask. rowsMask is a column matrix of type Matrix or VMatrix with a true value where the row should be selected",
     "Selecciona de la matrix M las filas indicadas en indicadoresDeFila. indicadoresDeFila es una matriz columna de tipo Matrix o VMatrix con valores verdadero en las filas que se quieren seleccionar."),
    BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BVMatExtractRowByMask::CalcContens()
//--------------------------------------------------------------------
{
  BVMat& M = VMat(Arg(1));
  BArray<int> rows;
  if(GetIndexes(I2("rowsMask", "indicadoresDeFila"), Arg(2), rows))
    {
    M.SubRows(rows, contens_);
    assert(contens_.Check());
    }
}

//--------------------------------------------------------------------
DeclareContensClass(BVMat, BVMatTemporary, BVMatExtractColumnsByMask);
DefMethod(1, BVMatExtractColumnsByMask, "VMatSubColByMask", 2, 2, "VMatrix Anything",
  I2("(VMatrix M , Anything columnsMask)",
     "(VMatrix M , Anything indicadoresDeColumna)"),
  I2("Selects from the matrix M the indicated columns in columnsMask. columnsMask is a column matrix of type Matrix or VMatrix with a true value where the row should be selected",
     "Selecciona de la matrix M las filas indicadas en indicadoresDeColumns. indicadoresDeFila es una matriz columna de tipo Matrix o VMatrix con valores verdadero en las columnas que se quieren seleccionar."),
    BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BVMatExtractColumnsByMask::CalcContens()
//--------------------------------------------------------------------
{
  BVMat& M = VMat(Arg(1));
  BArray<int> cols;
  if(GetIndexes(I2("columsMask", "indicadoresDeColumna"), Arg(2), cols))
    {
    M.SubCols(cols, contens_);
    assert(contens_.Check());
    }
  assert(contens_.Check());
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatSqrDistanceOfColumns);
DefMethod(1, BMatSqrDistanceOfColumns, "SqrDistanceOfColumns", 1, 1, 
  "Matrix",
  "(Matrix X)",
  I2("Returns the matrix of squares of distances between columns of "
     "a given matrix",
     "Devuelve la matrix M de los cuadrados de las distancias entre "
     "las columnas de una matriz."),
  BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatSqrDistanceOfColumns::CalcContens()
//--------------------------------------------------------------------
{
  BMat& X = Mat(Arg(1));
  int i,j,k,r = X.Rows(), c=X.Columns();
  contens_.Alloc(c,c);
  for(i=0; i<c; i++)
  {
    contens_(i,i) = 0;
    for(j=0; j<i; j++)
    {
      BDat z = 0;
      for(k=0; k<r; k++)
      {
        z += (X(k,i)-X(k,j))^2;
      }
      contens_(j,i) = contens_(i,j) = z;
    }
  }
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatSqrDistanceOfColumns2);
DefMethod(1, BMatSqrDistanceOfColumns2, "SqrDistanceOfColumns2", 2, 2, 
  "Matrix Matrix",
  "(Matrix X, Matrix Y)",
  I2("Returns the matrix of squares of distances between columns of "
  "with the same number of rowsa pair of matrices",
  "Devuelve la matrix M de los cuadrados de las distancias entre "
  "las columnas de un par de matrices con el mismo n�mero de filas."),
  BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatSqrDistanceOfColumns2::CalcContens()
//--------------------------------------------------------------------
{
  BMat& X = Mat(Arg(1));
  BMat& Y = Mat(Arg(2));
  int i,j,k,r = X.Rows(), c=X.Columns(), d = Y.Columns();
  if(r!=Y.Rows()) 
  { 
    Error(BText("[SqrDistanceOfColumns2] Two matrices must have the "
    "same number of rows"));
    return; 
  }
  contens_.Alloc(c,d);
  for(i=0; i<c; i++)
  {
    for(j=0; j<d; j++)
    {
      BDat z = 0;
      for(k=0; k<r; k++)
      {
        z += (X(k,i)-Y(k,j))^2;
      }
      contens_(i,j) = z;
    }
  }
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatDifEqJeuland);
DefMethod(1, BMatDifEqJeuland, "DifEq.Jeuland", 5, 5, 
  "Matrix Matrix Matrix Real Real",
  "(Matrix m, Matrix p, Matrix q, Real s0, Real gamma)",
  "Solves the non linear equation in differences of Jeulan difussion "
  "process with dynamical parameters. :\n"
  "  S[t+1]-S[t] = (p[t]+q[t]*S[t]/m[t])*(m[t]-S[t])^(1+gamma)\n"
  "Subject to  \n"
  "  0<=p[t],q[t]<=1\n"
  "  0<=p[t]+q[t]<=1\n"
  "  0<=S[t]<=S[t+1]<=m[t+1]\n"
  "  0<=gamma\n"
  "Note that if gamma=0 then it Jeuland difussion becomes the Bass model",
  BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatDifEqJeuland::CalcContens()
//--------------------------------------------------------------------
{
  BMat& M = Mat(Arg(1));
  BMat& P = Mat(Arg(2));
  BMat& Q = Mat(Arg(3));
  BDat  S0 = Dat(Arg(4));
  BDat& gamma = Dat(Arg(5));
  int k, r = P.Rows(), c=P.Columns();
  if((r!=Q.Rows())|(r!=M.Rows())|(c!=1)|(c!=Q.Columns())|(c!=M.Columns()))
  { 
    Error(BText("[BMatDifEqJeuland] All matrices must have the same number of rows and just one column"));
    return; 
  }
  contens_.Alloc(r,1);
  BMat& S = contens_;
  BDat* s = S.GetData().GetBuffer();
  const BDat* m = M.Data().Buffer();
  const BDat* p = P.Data().Buffer();
  const BDat* q = Q.Data().Buffer();
  BDat h,st=S0;
  for(k=0; k<r; k++, m++, p++, q++, s++)
  {
    h = (*p+*q * st / *m) * (*m - st);
    *s = st+((h>0.0)?h:0.0);
    st = *s;
  }
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMatLastMax);
DefMethod(1, BMatLastMax, "LastMax", 1, 1, 
  "Matrix",
  "(Matrix A)",
  "Returns the max value of all cells before each one in each row of "
  "given matrix A.",
  BOperClassify::MatrixAlgebra_);
//--------------------------------------------------------------------
void BMatLastMax::CalcContens()
//--------------------------------------------------------------------
{
  BMat& A = Mat(Arg(1));
  int k;
  int r = A.Rows();
  int c = A.Columns();
  int s = r*c;
  contens_.Alloc(r,c);
  const BDat* a = A.Data().Buffer();
  BDat* x = contens_.GetData().GetBuffer();
  BDat max = BDat::NegInf();
//Std(BText("[BMatLastMax] ")<<" r="<<r<<" c="<<c<<" s="<<s);
  for(k=0; k<s; k++, x++, a++)
  {
    if(!(k%c)) { max = BDat::NegInf(); }
    if(*a>max) { max = *a; }
  //Std(BText("[BMatLastMax] ")<<" k="<<k<<" a="<<*a<<" max="<<max<<"\n");
    *x = max;
  }
}
