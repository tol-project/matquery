/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : MatQuery.tol
// PURPOSE: Defines package MatQuery

//////////////////////////////////////////////////////////////////////////////
NameBlock MatQuery = 
//////////////////////////////////////////////////////////////////////////////
[[
  Text _.autodoc.name = "MatQuery";
  Text _.autodoc.brief = "Query tools for matrices";
  Text _.autodoc.description = "Functions and classes related with efficient "
    "selection and classification methods over large matricial data.";
  Text _.autodoc.url = 
    "http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
  Set _.autodoc.keys = [["select","classify"]];
  Set _.autodoc.authors = [["vdebuen@tol-project.org"]];
  Text _.autodoc.minTolVersion = "v3.2"; 
  Text _.autodoc.platform = 
    TolPackage::TolPlatform::ObtainPlatform("OWN", "OWN", "OWN");
  Text _.autodoc.variant = 
    TolPackage::TolPlatform::ObtainVariant(_.autodoc.platform);
  Real _.autodoc.version.high = 6;
  Real _.autodoc.version.low = 3;
  Set _.autodoc.dependencies = Copy(Empty);
  Set _.autodoc.nonTolResources = [[
    TolPackage::TolPlatform::ObtainDllFolder(?)
  ]];
  Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));
  
  #Embed "select.tol";
  #Embed "classify.tol";
  #Embed "sort.tol";
  #Embed "KNN.tol";
  #Embed "TPP.tol";
  #Embed "CsvTools.tol";
  #Embed "DataLoader.tol";
  
  #Embed "CppTools.tol";
  
  NameBlock CppTools = [[ Real unused = ?]];
  
  Real _is_started = False;
  
  Text _dllFile = "";
  
  ////////////////////////////////////////////////////////////////////////////
  Text _DllFile(Real forceRelease)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text dllPath.rel = GetAbsolutePath(
      TolPackage::TolPlatform::ObtainDllFolder(?))+"/";
    dllPath.rel+Name(_this)+"."+GetSharedLibExt(0)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text DllFile(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text try = _DllFile(false);
    If(OSFilExist(try), try, _DllFile(true))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real StartActions(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_is_started, False, {
      Real _is_started := True;
      Text _dllFile := DllFile(?);
      NameBlock CppTools := LoadDynLib(_dllFile);
      Real _CppTools.StartActions(?);
      True
    })
  }
]];
//////////////////////////////////////////////////////////////////////////////
